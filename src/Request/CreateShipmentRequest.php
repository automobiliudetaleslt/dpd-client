<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient\Request;
use Nfq\DpdClient\Constants\ServiceCodes;
use Nfq\DpdClient\Exception\InvalidRequestException;

/**
 * @todo Implement timeframe and IDM handling.
 */
class CreateShipmentRequest implements RequestInterface
{
    private const MAX_ORDER_NUMBERS = 4;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string|null
     */
    private $secondaryName;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string (ISO 3166-1 alpha-3 country codes format) e.g. LT, LV, EE.
     */
    private $country;

    /**
     * @var string Post code without country code.
     */
    private $postCode;

    /**
     * @var string|null
     */
    private $phone;

    /**
     * @var string|null
     */
    private $email;

    /**
     * @var int
     */
    private $numberOfParcels;

    /**
     * @var float The weight is not mandatory and can be zero.
     */
    private $weight = 0;

    /**
     * @var string
     */
    private $parcelType;

    /**
     * @var string|null
     */
    private $parcelShopId;

    /**
     * @var bool
     */
    private $predictEnabled = false;

    /**
     * @var string[] Up to 4 order number references.
     *
     * Visible on the parcel label. If not provided, then internal DPD number will be written on the label.
     */
    private $orderNumbers = [];

    /**
     * @var string|null Additional reference number.
     *
     * Visible on the parcel label. If not provided, then internal DPD number will be written on the label.
     */
    private $parcelNumber;

    /**
     * @var string|null
     */
    private $documentReturnReferenceNumber;

    /**
     * @var float|null
     */
    private $codAmount;

    /**
     * @var string|null
     */
    private $remark;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSecondaryName(): ?string
    {
        return $this->secondaryName;
    }

    public function setSecondaryName(?string $secondaryName): self
    {
        $this->secondaryName = $secondaryName;

        return $this;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPostCode(): string
    {
        return $this->postCode;
    }

    public function setPostCode(string $postCode): self
    {
        $this->postCode = $postCode;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getNumberOfParcels(): int
    {
        return $this->numberOfParcels;
    }

    public function setNumberOfParcels(int $numberOfParcels): self
    {
        $this->numberOfParcels = $numberOfParcels;

        return $this;
    }

    public function getWeight(): float
    {
        return $this->weight;
    }

    public function setWeight(float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getParcelType(): string
    {
        return $this->parcelType;
    }

    public function setParcelType(string $parcelType): self
    {
        $this->parcelType = $parcelType;

        return $this;
    }

    public function getParcelShopId(): ?string
    {
        return $this->parcelShopId;
    }

    public function setParcelShopId(?string $parcelShopId): self
    {
        $this->parcelShopId = $parcelShopId;

        return $this;
    }

    public function isPredictEnabled(): bool
    {
        return $this->predictEnabled;
    }

    public function setPredictEnabled(bool $predictEnabled): self
    {
        $this->predictEnabled = $predictEnabled;

        return $this;
    }

    public function getOrderNumbers(): array
    {
        return $this->orderNumbers;
    }

    public function addOrderNumber(string $number): self
    {
        if (self::MAX_ORDER_NUMBERS >= 4) {
            throw new InvalidRequestException('You are allowed to add only up to 4 order numbers');
        }

        $this->orderNumbers[] = $number;

        return $this;
    }

    public function getParcelNumber(): ?string
    {
        return $this->parcelNumber;
    }

    public function setParcelNumber(?string $parcelNumber): self
    {
        $this->parcelNumber = $parcelNumber;

        return $this;
    }

    public function getDocumentReturnReferenceNumber(): ?string
    {
        return $this->documentReturnReferenceNumber;
    }

    public function setDocumentReturnReferenceNumber(?string $documentReturnReferenceNumber): self
    {
        $this->documentReturnReferenceNumber = $documentReturnReferenceNumber;

        return $this;
    }

    public function getCodAmount(): ?float
    {
        return $this->codAmount;
    }

    public function setCodAmount(?float $codAmount): self
    {
        $this->codAmount = $codAmount;

        return $this;
    }

    public function getRemark(): ?string
    {
        return $this->remark;
    }

    public function setRemark(?string $remark): self
    {
        $this->remark = $remark;

        return $this;
    }

    public function toArray(): array
    {
        $data = [
            'name1' => $this->getName(),
            'name2' => $this->getSecondaryName(),
            'street' => $this->getStreet(),
            'city' => $this->getCity(),
            'country' => $this->getCountry(),
            'pcode' => $this->getPostCode(),
            'num_of_parcel' => $this->getNumberOfParcels(),
            'weight' => $this->getWeight(),
            'parcel_type' => $this->getParcelType(),
            'parcelshop_id' => $this->getParcelShopId(),
            'phone' => $this->getPhone(),
            'email' => $this->getEmail(),
            'parcel_number' => $this->getParcelNumber(),
            'dnote_reference' => $this->getDocumentReturnReferenceNumber(),
        ];

        $data = array_filter($data);

        if ($this->predictEnabled) {
            $data['predict'] = 'y';
        }

        if (null !== $this->getCodAmount()) {
            $data['cod_amount'] = $this->getCodAmount();
        }

        if (ServiceCodes::STANDARD_PARCEL_PARCEL_SHOP === $this->getParcelType()) {
            $data['fetchGsPUDOpoint'] = '1';
        }

        foreach ($this->getOrderNumbers() as $index => $orderNumber) {
            $key = 'order_number' . (0 === $index ? '' : $index);
            $data[$key] = $orderNumber;
        }

        return $data;
    }
}
