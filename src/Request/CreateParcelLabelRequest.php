<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient\Request;

class CreateParcelLabelRequest implements RequestInterface
{
    /**
     * @var string[]
     */
    private $parcelNumbers;

    /**
     * @var string|null
     */
    private $printFormat;

    /**
     * @var string|null
     */
    private $printSize;

    /**
     * @return string[]
     */
    public function getParcelNumbers(): array
    {
        return $this->parcelNumbers;
    }

    /**
     * @param string[] $parcelNumbers
     *
     * @return $this
     */
    public function setParcelNumbers(array $parcelNumbers): self
    {
        $this->parcelNumbers = $parcelNumbers;

        return $this;
    }

    public function getPrintFormat(): ?string
    {
        return $this->printFormat;
    }

    public function setPrintFormat(?string $printFormat): self
    {
        $this->printFormat = $printFormat;

        return $this;
    }

    public function getPrintSize(): ?string
    {
        return $this->printSize;
    }

    public function setPrintSize(?string $printSize): self
    {
        $this->printSize = $printSize;

        return $this;
    }

    public function toArray(): array
    {
        $data = [
            'parcels' => implode('|', $this->getParcelNumbers()),
            'printType' => $this->getPrintFormat(),
            'printFormat' => $this->getPrintSize(),
        ];

        return array_filter($data);
    }
}
