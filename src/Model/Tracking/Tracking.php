<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient\Model\Tracking;

class Tracking
{
    /** @var string */
    private $parcelNumber;

    /** @var array */
    private $details;

    /** @var Error|null */
    private $error;

    /**
     * @return string
     */
    public function getParcelNumber(): string
    {
        return $this->parcelNumber;
    }

    /**
     * @param string $parcelNumber
     *
     * @return Tracking
     */
    public function setParcelNumber(string $parcelNumber): Tracking
    {
        $this->parcelNumber = $parcelNumber;

        return $this;
    }

    /**
     * @return array
     */
    public function getDetails(): array
    {
        return $this->details;
    }

    /**
     * @param array $details
     *
     * @return Tracking
     */
    public function setDetails(array $details): Tracking
    {
        $this->details = $details;

        return $this;
    }

    /**
     * @return Error|null
     */
    public function getError(): ?Error
    {
        return $this->error;
    }

    /**
     * @param Error|null $error
     *
     * @return Tracking
     */
    public function setError(?Error $error): Tracking
    {
        $this->error = $error;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasError(): bool
    {
        return !$this->isValid();
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->getError() === null;
    }
}
